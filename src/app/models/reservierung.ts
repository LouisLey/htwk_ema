export interface Reservierung {
    error: string;
    reservations?: (ReservationsEntity)[] | null;
}

export interface ReservationsEntity {
    res_ID: string;
    Von: string;
    tvon: string;
    Bis: string;
    tbis: string;
    Bemerkung: string;
    nutz_ID: string;
    Name: string;
    Vorname: string;
    Email: string;
}
