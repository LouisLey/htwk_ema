export interface MyRoom {
    error: string;
    myrooms?: (MyroomsEntity)[] | null;
    rooms?: (null)[] | null;
}
export interface MyroomsEntity {
    ID: string;
    Bez: string;
    Rnr: string;
    Frei: string;
    Nutzer_ID: string;
}
