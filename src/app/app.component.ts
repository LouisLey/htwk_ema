import {AfterViewInit, Component, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {firebase} from '@firebase/app';
import {environment} from '../environments/environment';
import {NotificationsService} from './notifications.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit, AfterViewInit {
    public appPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home'
        },
        {
            title: 'List',
            url: '/list',
            icon: 'list'
        },
        {
            title: 'Raumplanung',
            url: '/raumplanung',
            icon: 'ice-cream'
        },
        {
            title: 'Mobile Guide',
            url: '/mobile-guide',
            icon: 'locate'
        }
    ];

    async ngOnInit() {
        firebase.initializeApp(environment.firebase);
        await this.notificationsService.init();
    }

    ngAfterViewInit() {
        this.platform.ready().then(async () => {
            await this.notificationsService.requestPermission();
        });
    }

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private notificationsService: NotificationsService,
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}
