import {Component, OnInit} from '@angular/core';
import {MenuEntity, Menus} from '../models/menus';
import {LoadingController} from '@ionic/angular';
import {MensaService} from '../service/mensa.service';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    menus: Menus;
    menuEntities: MenuEntity[];
    essen: any[] = [];
    myDate = new Date();

    ngOnInit(): void {
        this.menus = null;
        this.loadFood();
        // this.configurePush();
    }

    // todo push funktioniert zwar aber die App rednert nichts
//     private async configurePush() {
//         // to check if we have permission
//         this.push.hasPermission()
//             .then((res: any) => {
//
//                 if (res.isEnabled) {
//                     console.log('We have permission to send push notifications');
//                 } else {
//                     console.log('We do not have permission to send push notifications');
//                 }
//
//             });
//
// // to initialize push notifications
//         const options: PushOptions = {
//             android: {},
//             ios: {
//                 alert: 'true',
//                 badge: true,
//                 sound: 'false'
//             },
//             windows: {},
//             browser: {
//                 pushServiceURL: 'http://push.api.phonegap.com/v1/push'
//             }
//         };
//
//         const pushObject: PushObject = this.push.init(options);
//
//         pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
//
//         pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
//
//         pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
//     }


    constructor(
        private http: HttpClient,
        private loadingCtrl: LoadingController,
        private mensaService: MensaService,
        // private push: Push
    ) {
    }

    private async loadFood() {
        const service = this.mensaService.getEssen();
        const loading = await this.loadingCtrl.create({
            message: 'Please wait...',
            spinner: 'crescent',
            duration: 2000
        });

        loading.present();
        service.subscribe(res => {
            this.menuEntities = res.Eintrag;
            this.essen = this.menuEntities.filter(test => {
                // console.log(new Date(test.Datum).toISOString());
                console.log(this.myDate.toISOString());
                return new Date(test.Datum).setHours(0, 0, 0, 0) === this.myDate.setHours(0, 0, 0, 0);
            });
            console.log(this.essen);
            loading.dismiss();
        }, err => {
            this.menuEntities = [];
            loading.dismiss();
        });
    }

    public nextDay() {
        this.myDate.setDate(this.myDate.getDate() + 1);
        console.log(this.myDate.getDay());
        this.essen = this.menuEntities.filter(test => {
            console.log(this.myDate.toISOString());
            return new Date(test.Datum).setHours(0, 0, 0, 0) === this.myDate.setHours(0, 0, 0, 0);
        });
        console.log(this.essen);
    }

    public previousDay() {
        this.myDate.setDate(this.myDate.getDate() - 1);
        this.essen = this.menuEntities.filter(test => {
            console.log(this.myDate.toISOString());
            return new Date(test.Datum).setHours(0, 0, 0, 0) === this.myDate.setHours(0, 0, 0, 0);
        });
        console.log(this.essen);
    }
}
