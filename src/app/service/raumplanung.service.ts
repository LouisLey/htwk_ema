import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {User} from '../models/User';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Room} from '../models/Room';
import {Reservierung} from '../models/reservierung';
import {MyRoom} from '../models/MyRoom';

// Injectable marks it as a service that can be injected
@Injectable({
    providedIn: 'root'
})
export class RaumplanungService {

    constructor(
        private http: HttpClient
    ) {
    }

    private readonly baseUrl: string = 'https://digiboard.htwk-leipzig.de/raumplanung';

    static handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

    register(user: User) {
        const url = this.baseUrl + '/register.php' + '?email=' + user.email + '&name=' + user.name + '&vorname=' + user.vorname;
        console.log(url);
        return this.http.get<User>(url).pipe(catchError(RaumplanungService.handleError));
    }

    addRoom(room: Room) {
        /*did: Device-ID
        bez: Raumbezeichnung (etwas länger)
        nr: Raumnummer (die an der Tür steht)*/
        // const url = this.baseUrl + '/addroom.php' + '?did=' + user.deviceid + '&bez=' + room.bez + '&nr=' + room.nr;
        // console.log(url);
        return this.http.post(this.baseUrl + '/addroom.php', room).pipe(catchError(RaumplanungService.handleError));
    }

    getReservationsWithRoomID(rid: string, did: string) {
        const url = this.baseUrl + '/getreservations.php' + '?rid=' + rid + '&did=' + did;
        // https://digiboard.htwk-leipzig.de/raumplanung/getreservations.php?rid=1&did=5ce683ac30c6c8.63476780
        console.log(url);
        return this.http.get<Reservierung>(url).pipe(catchError(RaumplanungService.handleError));
    }

    getMyRooms(did: string) {
        const url = this.baseUrl + '/getrooms.php' + '?did=' + did;
        // https://digiboard.htwk-leipzig.de/raumplanung/getrooms.php?did=5ce683ac30c6c8.63476780
        return this.http.get<MyRoom>(url).pipe(catchError(RaumplanungService.handleError));
    }


    postReservation(did: string, von: string, bis: string, nr: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        // tslint:disable-next-line:max-line-length
        const url = this.baseUrl + '/addreservation.php' + '?did=' + did + '&von=' + new Date(von).getTime() / 1000 + '&bis=' + new Date(bis).getTime() / 1000 + '&nr=' + nr;
        console.log(url);
        return this.http.post(url, {}, httpOptions).pipe(catchError(RaumplanungService.handleError));
    }
}
