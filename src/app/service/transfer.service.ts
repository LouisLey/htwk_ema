import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../models/User';

@Injectable()
export class TransfereService {

    constructor(
        private router: Router,
    ) {
    }

    private user: User;

    setData(user) {
        this.user = user;
    }

    getData() {
        const temp = this.user;
        this.clearData();
        return temp;
    }

    clearData() {
        this.user = undefined;
    }

}
