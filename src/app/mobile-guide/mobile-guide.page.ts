import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NavController} from '@ionic/angular';
import leaflet from 'leaflet';

@Component({
    selector: 'app-mobile-guide',
    templateUrl: './mobile-guide.page.html',
    styleUrls: ['./mobile-guide.page.scss'],
})
export class MobileGuidePage implements OnInit {

    constructor(public navCtrl: NavController) {

    }
    @ViewChild('map') mapContainer: ElementRef;
    map: any;

    markers = [
        {
            id: '3',
            name: 'Albertina',
            longitude: '12.368625',
            latitude: '51.332293',
            url: '',
            // tslint:disable-next-line:max-line-length
            text: '<p>Die Universit\u00e4tsbibliothek besteht aus der Hauptbibliothek "Bibliotheca Albertina" sowie 20 Zweigbibliotheken in der N\u00e4he der wissenschaftlichen Einrichtungen der Universit\u00e4t Leipzig. Die Albertina stellt dabei nicht nur die Hauptanlaufstelle von tausenden Studenten f\u00fcr ihre Recherchen und Studien dar, sondern ist auch ein wichtiger Bestandteil des studentischen Lebens der Stadt Leipzig. Ein atemberaubendes Flair, dass von dem Geb\u00e4ude und seiner Architektur ausgeht, sowie die vielen M\u00f6glichkeiten innerhalb des Bibliothekkomplexes, machen die Albertina zu der Bibliothek, die wir heute kennen.<\/p>\n',
            kategorie: '2',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:29:23'
        },
        {
            id: '4',
            name: 'Sternwarte Plei\u00dfenburg',
            longitude: '12.372423',
            latitude: '51.336388',
            url: '',
            // tslint:disable-next-line:max-line-length
            text: '<p>W\u00e4hrend seiner Rektoratszeit 1780\/81 war Georg Heinrich Borz bestrebt, den Aufbau einer Beobachtungsstation an der Leipziger Universit\u00e4t nach dem Vorbild von Kurf\u00fcrst Friedrich August I. zu erreichen. 1794 wurde das Unternehmen vom Architekten Johann Carl Friedrich Dauthe an der Plei\u00dfenburg fertiggestellt. Der erste Observator Christian Friedrich R\u00fcdiger baute bereits eine umfassende Bibliothek auf, welche um 1843 mehr als 2000 mathematische und astronomische Werke enthielt. In den Jahren 1818 bis 1821 wurde die Sternwarte komplett erneuert. So wurde beispielsweise ein eigener Raum f\u00fcr Meridianbeobachtungen und neue Teleskope angelegt.Die mittelalterliche Plei\u00dfenburg wurde zusammen mit der Sternwarte 1897 abgerissen, um Platz f\u00fcr das Neue Rathaus zu schaffen. Die Fundamente des alten Burgturmes wurden f\u00fcr die Errichtung des neuen Rathausturmes genutzt.<\/p>\n',
            kategorie: '2',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:13:11'
        },
        {
            id: '5',
            name: 'Sternwarte Johannistal',
            longitude: '12.388316',
            latitude: '51.333655',
            url: '',
            // tslint:disable-next-line:max-line-length
            text: '<p>Die Bauarbeiten am Johannistal begannen mit der Amtsperiode von Karl Christian Bruhns als Direktor der Sternwarte um 1860. Verantwortlich f\u00fcr den Bau waren der Leipziger Architekt Albert Geutebr\u00fcck und der Baumeister R. Lucae aus Berlin, die sich an der Bauart der Berliner, Kuppel der Bonner sowie Klappen der Gothaer Sternwarte orientierten. Zur Er\u00f6ffnung am 8. November 1861 wurde der Bestand um ein Fernrohr, zwei Pendeluhren, ein Registrierapparat und andere Ger\u00e4te erweitert. Die Bibliothek umfasste zum Zeitpunkt der \u00dcbergabe an die Sternwarte Bamberg im Jahr 1896 rund 3500 Werke der Astronomie. 1863 w\u00e4hlte der internationale Zusammenschluss von Astronomen als Sitz Leipzig. Mit der Begr\u00fcndung der astronomischen Schule, sowie der Astrophysik als wissenschaftliche Disziplin durch Bruhns stieg der Einfluss der Leipziger auf die Gesamtentwicklung der Astronomie enorm. Am 4. April 1955 l\u00f6ste man die Sternwarten vom Physikalischen Institut und gliederte sie dem Geophysikalischen Institut an. Im Johannistal richtete sich dann das Meteorologische Institut und ein Bauhof der Universit\u00e4t ein.<\/p>\n',
            kategorie: '2',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:18:35'
        },
        {
            id: '6',
            name: 'Universit\u00e4tskliniken',
            longitude: '12.386237',
            latitude: '51.33138',
            url: '',
            text: '',
            kategorie: '2',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-27 04:13:28'
        },
        {
            id: '7',
            name: 'HTWK',
            longitude: '12.373094',
            latitude: '51.314297',
            url: 'http:\/\/stadt-leipzig.htwk-leipzig.de\/hochschulen\/index.php\/htwk.html',
            // tslint:disable-next-line:max-line-length
            text: '<p>Informationen zur HTWK finden Sie unter folgendem Link:<\/p>\n\n<p>http:\/\/stadt-leipzig.htwk-leipzig.de\/hochschulen\/index.php\/htwk.html<\/p>\n',
            kategorie: '2',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:22:22'
        },
        {
            id: '8',
            name: 'Innenstadt',
            longitude: '12.376009',
            latitude: '51.339856',
            url: '',
            text: '<p>Innenstadt Leipzig<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:25:24'
        },
        {
            id: '9',
            name: 'Reichsgericht',
            longitude: '12.370284',
            latitude: '51.333059',
            url: '',
            // tslint:disable-next-line:max-line-length
            text: '<p>Zwischen 1888 und 1895 erreichtet, orientiert sich das Reichsgerichtsgeb\u00e4ude bautechnisch an der italienischen Sp\u00e4trenaissance sowie dem franz\u00f6sischen Barock. Von 1998 bis 2001 aufw\u00e4ndig saniert, befindet sich in den R\u00e4umlichkeiten seit August 2002 das Bundesverwaltungsgericht.Vor der Hauptfront des Geb\u00e4udes erstreckt sich der nach dem ersten Pr\u00e4sidenten des Reichsgerichts benannte Eduard von Simson Platz. In der aufw\u00e4ndig gestalteten Kuppelhalle im Eingangsbereich werden die Besucher von dekorativen Bogenfeldern mit Reliefplastiken empfangen. Auch die zahlreichen Sitzungss\u00e4le sind mit aufwendigen Reliefs, Verzierungen und Gem\u00e4lden von den Begr\u00fcndern des ehemaligen Reichsgerichts dekoriert. Der idyllische Gartenhof im Ostfl\u00fcgel verbindet die alten und neu errichteten Elemente des Geb\u00e4udes. Ein moderner verglaster Aufzug f\u00fchrt zu einer nachtr\u00e4glich aufgesetzten Etage, welche sich hinter der prachtvollen Fassade des Geb\u00e4udes versteckt. Interessierten Besuchern bietet sich die M\u00f6glichkeit an F\u00fchrungen teilzunehmen und eine Dauerausstellung zum Thema "Das Reichsgerichtsgeb\u00e4ude und seine Nutzer" zu besuchen.<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:32:38'
        },
        {
            id: '10',
            name: 'M\u00e4dler Passage',
            longitude: '12.37544',
            latitude: '51.339621',
            url: '',
            // tslint:disable-next-line:max-line-length
            text: '<p>Heinrich Stromer erschuf bis 1938 einen der prunkvollsten, sicher aber den ber\u00fchmtesten Handelshof der Leipziger Innenstadt, den Auerbachs Hof. Seine Ber\u00fchmtheit erreichte das Geb\u00e4ude vor allem durch seine Erw\u00e4hnung in Goethes "Faust I" und hat sie sich bis heute erhalten. Oberirdisch findet der Besucher sich in einem 140 m langen, verwinkelten Durchgang wieder, der Zutritt zu 100 Messegew\u00f6lben sowie weiteren S\u00e4len und Zimmern gew\u00e4hrt.<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:48:18'
        },
        {
            id: '11',
            name: 'K\u00f6nigshaus Passage',
            longitude: '12.375093',
            latitude: '51.33947',
            url: '',
            text: '<p>An der S\u00fcdseite des Leipziger Marktes steht das ehemalige Apelsches oder Apelshaus, heute bekannter unter dem Namen K\u00f6nighaus. Errichtet wurde das B\u00fcrgerhaus wahrscheinlich um 1558. Aus dieser Zeit ist noch das Erdgeschoss mit einer gro\u00dfen Kreuzgew\u00f6lbehalle erhalten. Das alle benachbarten H\u00e4user \u00fcberragende Geb\u00e4ude, wurde vom Rat der Stadt seit dem 16. Jahrhundert immer wieder als G\u00e4stehaus gemietet. Das K\u00f6nigshaus hatte eine bewegte Geschichte. Hohe Herrschaften gingen hier ein und aus. Gespr\u00e4che und Verhandlungen, welche die Welt ver\u00e4nderten wurden hier gehalten.<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:37:07'
        },
        {
            id: '12',
            name: 'Barthels Hof',
            longitude: '12.374035',
            latitude: '51.341227',
            url: '',
            text: '<p>Zwischen Hainstra\u00dfe und Kleiner Fleischergasse erstreckt sich der Barthels Hof, der ohne Kompromisse als eine der wichtigsten Sehensw\u00fcrdigkeiten Leipzigs genannt werden kann. Nicht nur, weil er der letzte noch erhaltene Durchgangshof der Stadt Leipzig ist, sondern schlichtweg auch seiner Sch\u00f6nheit wegen.<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 11:46:07'
        },
        {
            id: '13',
            name: 'Specks Hof',
            longitude: '12.377409',
            latitude: '51.340464',
            url: '',
            // tslint:disable-next-line:max-line-length
            text: '<p>Der Specks Hof ist die \u00e4lteste erhaltene Ladenpassage in Leipzig, direkt neben der Nikolaikirche, stand hier seit 1430 ein gro\u00dfes Wohnhaus mit eigenem Brauhaus und gro\u00dfem Weinkeller und insgesamt sieben Hinterh\u00e4usern. Heute beherbergt der Specks Hof zwei Dutzend Gesch\u00e4fte, darunter haupts\u00e4chlich edle Boutiquen, kleine L\u00e4den und Gastst\u00e4tten.<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 16:43:29'
        },
        {
            id: '14',
            name: 'Stentzlers Hof',
            longitude: '12.375324',
            latitude: '51.337295',
            url: '',
            text: '<p>Stentzlers Hof f\u00fcgt sich reich geschm\u00fcckt in das Leipziger Innenstadtbild ein. Erbaut 1914-1916 nach den Entw\u00fcrfen des Leipziger Architekten Leopold Stentzler ist er ein charakteristisches Beispiel f\u00fcr die Messebauten der Stadt. An der Ecke von Stentzlers Hof verk\u00f6rpert eine Rolandstatue mit Schwert und Leipziger Stadtwappen die Eigenst\u00e4ndigkeit der Stadt, die durch Marktrecht und eigene Gerichtsbarkeit Freiheit und Macht erlangte. Unter ihm verk\u00fcndet ein Spruchband: "Einigkeit macht stark"<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:20:34'
        },
        {
            id: '15',
            name: 'V\u00f6lkerschlachtdenkmal',
            longitude: '12.413225',
            latitude: '51.312568',
            url: '',
            text: '<p>Das V\u00f6lkerschlachtdenkmal ist die monumentalste Erinnerung an die V\u00f6lkerschlacht bei Leipzig (16.-18.Oktober 1813). Die Verb\u00fcndeten (Preu\u00dfen, Russland, \u00d6sterreich & Schweden) beendeten mit ihrem Sieg \u00fcber Napoleon dessen Fremdherrschaft \u00fcber Europa.<br \/>\n<br \/>\nDer von Clemens Thieme gegr\u00fcndete Deutsche Patrioten-Bund beauftragte Bruno Schmitz 1896 mit der Ausarbeitung eines Entwurfs f\u00fcr ein Nationaldenkmal. Er ist somit Architekt des V\u00f6lkerschlachtdenkmals.<br \/>\n<br \/>\nNach 15 Jahren Bauzeit (1898-1913) wurde das V\u00f6lkerschlachtdenkmal, genau 100 Jahre nach der V\u00f6lkerschlacht, von Kaiser Friedrich Wilhelm II. eingeweiht.<br \/>\n<br \/>\nDas V\u00f6lkerschlachtdenkmal z\u00e4hlt mit seiner H\u00f6he von 91m und 300.000t verbautem Material zu den gr\u00f6\u00dften Denkm\u00e4lern Europas. Es besteht zu 90% aus Beton und ist somit ein bedeutendes Werk aus der Fr\u00fchzeit der Betonbaukunst. Die vom Jugendstil angehauchten Plastiken stammen von den Bildhauern Christian Behrens und Franz Metzner.<br \/>\n<br \/>\nHeute wird das Denkmal vorwiegend als Sehensw\u00fcrdigkeit und Veranstaltungsort f\u00fcr Konzerte genutzt. Das Denkmal wird seit 2003 komplett saniert.<br \/>\n<br \/>\n\u00d6ffnungszeiten:<br \/>\nApril-Oktober: t\u00e4glich 10-18 Uhr<br \/>\nNovember-M\u00e4rz: t\u00e4glich 10-16 Uhr<br \/>\n<br \/>\nF\u00fchrungen: Donnerstags 14 Uhr<br \/>\n<br \/>\nEintrittspreise:<br \/>\nErwachsene: 6 Euro<br \/>\nerm\u00e4\u00dfigt: 4 Euro<br \/>\nKinder bis 6 Jahre frei<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:58:53'
        },
        {
            id: '16',
            name: 'Alte Waage',
            longitude: '12.374908',
            latitude: '51.341218',
            url: '',
            text: '<p>Alte Waage<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-29 00:06:35'
        },
        {
            id: '17',
            name: 'Oper Leipzig',
            longitude: '12.381295',
            latitude: '51.34008',
            url: '',
            text: '<p>Oper Leipzig<\/p>\n',
            kategorie: '4',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-27 16:34:39'
        },
        {
            id: '18',
            name: 'City Hochhaus',
            longitude: '12.379092',
            latitude: '51.337878',
            url: '',
            text: '<p>Das City-Hochhaus, im Volksmund auch gerne Uniriese genannt ist das h\u00f6chste und wahrscheinlich bekannteste Geb\u00e4ude Leipzigs. Zudem war es zur Fertigstellung 1972 und ein Jahr dar\u00fcber hinaus das h\u00f6chste Geb\u00e4ude Deutschlands. Ein Highlight des ehemaligen Sektionsgeb\u00e4udes der Uni Leipzig ist die Aussichtsplattform in der 31. Etage, sowie das h\u00f6chste Restaurant Leipzigs in circa 120m H\u00f6he. Die hochwertige Verkleidung aus Naturstein und Glas sowie die Form eines aufgeschlagenen Buches unterstreichen die heutige Nutzung als modernes B\u00fcrogeb\u00e4ude. Sechs Personenaufz\u00fcge verbinden die 35 Geschosse des Geb\u00e4udes, die eine Fl\u00e4che von insgesamt 27.125 qm bereitstellen. Das Geb\u00e4ude wurde mittlerweile an die US-Investmentbank Merril Lynch verkauft. Heutiger Hauptmieter ist u.a. der MDR, dessen Logo die beiden Hauptansichten pr\u00e4gen.<\/p>\n',
            kategorie: '3',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:40:26'
        },
        {
            id: '19',
            name: 'Europahaus',
            longitude: '12.381576',
            latitude: '51.337466',
            url: '',
            text: '<p>Im Gegensatz zum Kroch-Hochhaus weckte dieses Hochhaus Leipzigs kaum \u00f6ffentliches Interesse. Der erste Entwurf f\u00fcr das Geb\u00e4ude sah vor, dass der Turm urspr\u00fcnglich an der Ecke gebaut werden sollte, um als Scharnier zwischen Augustus- und Ro\u00dfplatz zu wirken. Auch die Fassade wurde \u00fcberarbeitet und dabei deutlich vereinfacht. Bei der Instandsetzung in den Jahren 1998\/99 wurde die Fassade repariert, Fenster erneuert und die B\u00fcros modernisiert.<\/p>\n',
            kategorie: '3',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 16:44:41'
        },
        {
            id: '20',
            name: 'Krochhochhaus',
            longitude: '12.380026',
            latitude: '51.339852',
            url: '',
            text: '<p>Das Kroch-Hochhaus war das erste Hochhaus der Stadt und geh\u00f6rte gleichzeitig zu den umstrittensten Projekten der 20er Jahre in Leipzig. Im Fr\u00fchjahr 1925 stellte der Bankier Hans Kroch seine Pl\u00e4ne \u00fcber den Neubau einer Bank in der Form eines Turmhauses erstmals der Stadt vor. Das Geb\u00e4ude ist an den Uhrturm Torre dell\'\'Orologio aus Venedig angelehnt. Dies sollte dem Geb\u00e4ude den Charakter eines \u00f6ffentlichen Symbols bzw. eines Stadthauses verleihen. Bis 2009 wurde das Geb\u00e4ude f\u00fcr 4,9 Mio. Euro saniert.<\/p>\n',
            kategorie: '3',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:41:44'
        },
        {
            id: '21',
            name: 'Wintergartenhochhaus',
            longitude: '12.383984',
            latitude: '51.342771',
            url: '',
            text: '<p>Wintergartenhochhaus<\/p>\n',
            kategorie: '3',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 16:47:20'
        },
        {
            id: '22',
            name: 'Auerbachskeller',
            longitude: '12.375423',
            latitude: '51.339332',
            url: '',
            text: '',
            kategorie: '7',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-29 00:11:42'
        },
        {
            id: '23',
            name: 'Bayerischer Bahnhof',
            longitude: '12.381875',
            latitude: '51.328736',
            url: '',
            text: '<p>Der Bayerische Bahnhof ist eines der imposantesten Bauwerke der Stadt Leipzig. Im Jahr 1842 erstmals in Betrieb genommen, hat sich der Bayerische Bahnhof seine Bedeutung f\u00fcr die Stadt bis heute bewahrt, auch wenn sich sein Gesicht und seine Nutzung gewandelt haben. Heute fungiert der Bayerische Bahnhof, der vom Architekten Eduard P\u00f6tzsch entworfen wurde, unter anderem als Gastst\u00e4tte und Veranstaltungsort. Au\u00dferdem ist er der Standort der gleichnamigen Station des City-Tunnels, der modernen unterirdischen Verkehrsanbindung Leipzigs, die bis Ende 2013 entsteht.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 15:18:57'
        },
        {
            id: '24',
            name: 'Zills\'s Tunnel',
            longitude: '12.373298',
            latitude: '51.340772',
            url: '',
            text: '<p>Zills\'s Tunnel<\/p>\n',
            kategorie: '7',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 17:02:30'
        },
        {
            id: '25',
            name: 'Bhf. Plagwitz',
            longitude: '12.321446',
            latitude: '51.326536',
            url: '',
            text: '<p>Im 19. Jahrhundert wurde im Westen Leipzigs in direkter Nachbarschaft zum Plagwitz-Lindenauer Bahnhof der Preu\u00dfischen Staatseisenbahn der Plagwitz-Lindenau S\u00e4chsische Staatsbahnhof errichtet. Mit Gr\u00fcndung der Deutschen Reichsbahn 1920 wurde aus den getrennten Bahnh\u00f6fen der Leipzig-Plagwitzer Industriebahnhof. Dieser spielte eine wichtige Rolle im Umschlag von landwirtschaftlichen Erzeugnissen und Braunkohle. Nahezu jeder dort ans\u00e4ssige Betrieb hatte eine eigene Gleisverbindung, um Rohstoffe und fertige Erzeugnisse von Plagwitz aus an die gro\u00dfen Rangierbahnh\u00f6fe Leipzig Wahren oder Leipzig Engelsdorf und von dort aus letztendlich an den Fernverkehr \u00fcbergeben zu k\u00f6nnen. Mit der Wende gab es einen Einbruch in der Industrie und die Auslastung tendierte gegen Null. Der Bahnhof Leipzig Plagwitz wurde zum reinen Durchfuhrbahnhof. Heute ist das Areal eine Personenverkehrsstation mit S-Bahn Verbindung.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:27:20'
        },
        {
            id: '26',
            name: 'Bhf. Engelsdorf',
            longitude: '12.477223',
            latitude: '51.343936',
            url: '',
            text: '<p>Der Rangierbahnhof Engelsdorf geh\u00f6rte einst zur s\u00e4chsischen Staatsbahn und war der gr\u00f6\u00dfte Umschlagbahnhof f\u00fcr G\u00fcter und Waren aus Richtung Dresden und B\u00f6hmen. Der gesamte G\u00fcterverkehr aus dem Osten wurde durch den Bahnhof Engelsdorf geleitet. Noch heute ist die Anlage ein bedeutender Rangierbahnhof im Ost-West Verkehr und erf\u00fcllt wichtige Funktionen in der Zugbildung.\u00a0<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 11:55:52'
        },
        {
            id: '27',
            name: 'Postbahnhof',
            longitude: '12.39834',
            latitude: '51.35248',
            url: '',
            text: '<p>Zur gleichen Zeit wie der Leipziger Hauptbahnhof wurde der Postbahnhof errichtet. Dieser war damals mit einer Fl\u00e4che von \u00fcber 58.000 Quadratmetern und 16 Bahnsteigen der gr\u00f6\u00dfte Postbahnhof weltweit. Vom Postbahnhof aus wurden Rangierloks zum Hauptbahnhof entsandt, um die an die Schnellz\u00fcge angekoppelten Postwagenwaggons abzuholen. Diese wurden auf direktem Weg in die Umschlaghalle geschoben und be-\/entladen. Zu DDR-Zeiten erfolgte im Postbahnhof ein Umschlag s\u00e4mtlicher Brief- und Paketsendungen aus Westdeutschland. Zu Beginn der 90er Jahre wurde der Betrieb schlagartig eingestellt. Die Geb\u00e4udeteile und Anlagen liegen seitdem brach und stehen unter Denkmalschutz.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:35:55'
        },
        {
            id: '28',
            name: 'Hauptbahnhof',
            longitude: '12.380952',
            latitude: '51.34444',
            url: '',
            text: '<p>Der Hauptbahnhof pr\u00e4gt das Stadtbild Leipzigs seit fast 100 Jahren und ist Dreh- und Angelpunkt des Nah- und Fernverkehrs. Er bietet neben seiner imposanten Architektur auch eine spannende historische Vergangenheit.\u00a0<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:20:07'
        },
        {
            id: '29',
            name: 'Dresdner Bahnhof',
            longitude: '12.380952',
            latitude: '51.34444',
            url: '',
            text: '<p>Im Jahre 1839 wurde der Dresdner Bahnhof, dessen Errichtung vom Leipziger Architekten Eduard P\u00f6tzsch projektiert wurde, in Leipzig nach \u00fcber einem Jahr Bauzeit eingeweiht. Mit einer L\u00e4nge von rund 54 und einer Breite von rund 26 Metern war das Hauptgeb\u00e4ude des Dresdner Bahnhofs eher klein angelegt. Das fiel auch den Betreibern auf, die bereits kurz nach der Er\u00f6ffnung erste Ausbauarbeiten durchf\u00fchrten. So erhielt die Anlage nachtr\u00e4glich unter anderem acht seitliche Anbauten und vier Eckt\u00fcrme. In den folgenden Jahren entwickelte sich der Eisenbahnverkehr rasant. In den 1860er Jahren entschied man sich angesichts steigender Frequentierung f\u00fcr einen kompletten Umbau. Dieser wurde unter Leitung des Dresdner Oberingenieurs Hermann P\u00f6ge unddes Leipziger Architekten E. Hetzel durchgef\u00fchrt.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:07:24'
        },
        {
            id: '30',
            name: 'Berliner Bahnhof',
            longitude: '12.3',
            latitude: '51.3',
            url: '',
            text: '<p>In den Jahren 1858 und 1859 fand der Bau des Berliner Bahnhofs statt. Der Standort wurde weit au\u00dferhalb der Stadt gew\u00e4hlt, sodass eine gro\u00dfz\u00fcgige Anlage errichtet werden konnte. Als einziger der sechs Bahnh\u00f6fe in Leipzig ist der Berliner Bahnhof kein Kopfbahnhof im eigentlichen Sinne, sondern wurde auch als Durchgangsbahnhof, beispielsweise f\u00fcr eine Verbindungsbahn bis zum Bayerischen Bahnhof, genutzt. Im Jahr 1912, am 01. Oktober, wurde der Berliner Bahnhof geschlossen und abgerissen.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 11:54:59'
        },
        {
            id: '31',
            name: 'Eilenburger Bahnhof',
            longitude: '12.39539',
            latitude: '51.335315',
            url: '',
            text: '<p>Der Eilenburger Bahnhof entstand in den Jahren 1874 bis 1876 als letzter der sechs alten Leipziger Bahnh\u00f6fe. Bereits 1874 wurde der Betrieb aufgenommen, der Bahnhof bildete den Ausgangspunkt der Zweigbahn nach Eilenburg. Der Bahnhofsbau selbst wurde in den folgenden Monaten unter Projektierung des s\u00e4chsischen Architekten Richard Steche fortgesetzt und abgeschlossen. Ab 1915 \u00fcbernahm der Leipziger Hauptbahnhof den Fernverkehr des Eilenburger Bahnhofs. 1942 wurde der Personenverkehr eingestellt. Die Bahnhofsanlagen selbst fielen im Zweiten Weltkrieg Bombenangriffen zum Opfer.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:05:59'
        },
        {
            id: '32',
            name: 'Thueringer Bahnhof',
            longitude: '12.380952',
            latitude: '51.34444',
            url: '',
            text: '<p>Erst knapp zehn Jahre nach Er\u00f6ffnung des Bayerischen Bahnhofs im S\u00fcdosten Leipzigs wurde im Jahr 1857 der Th\u00fcringer Bahnhof fertiggestellt.<\/p>\n\n<p>Als Ausgangspunkt der Strecke Leipzig-Wei\u00dfenfels-Corbetha-Weimar-Erfurt wurde er als letzter der drei Bahnh\u00f6fe in der Innenstadt errichtet und sollte \u00fcber 50 Jahre bestehen bleiben. Am 01.Oktober 1907 wurde als erster der drei benachbarten Bahnh\u00f6fe der Th\u00fcringer geschlossen, unmittelbar darauf erfolgte der Abriss. Fast an gleicher Stelle begann der Hauptbahnhofsbau mit dem westlichen Teil, dem preu\u00dfischen Empfangsgeb\u00e4ude. Heute befindet sich am ehemaligen Standort des Th\u00fcringer Bahnhofs das Hotel Astoria, das etwa 300 Meter vom Hauptbahnhof entfernt ist.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:05:37'
        },
        {
            id: '33',
            name: 'Magdeburger Bahnhof',
            longitude: '12.380952',
            latitude: '51.34444',
            url: '',
            text: '<p>In den Jahren 1907-1912 \u00fcbernahm der Magdeburger Bahnhof die Aufgaben des Th\u00fcringer Bahnhofs, welcher bereits 1907 abgerissen wurde. 1912 folgte der Abbruch des Magdeburger Bahnhofs. Anschlie\u00dfend wurden die Arbeiten am Hauptbahnhof Leipzig begonnen. Nur ein Jahr sp\u00e4ter musste f\u00fcr dieses aufw\u00e4ndige Projekt auch der Dresdner Bahnhof weichen. Heute befinden sich auf dem Gel\u00e4nde des fr\u00fcheren Magdeburger Bahnhofs in etwa die Gleise 8 bis 15 des Leipziger Hauptbahnhofs.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:50:40'
        },
        {
            id: '34',
            name: 'City-Tunnel',
            longitude: '12.374692',
            latitude: '51.340966',
            url: '',
            text: '<p>Bereits im Jahr 1914 verfolgte Regierungsbaurat Georg Lubowski die Errichtung eines Tunnels. Im Gegensatz zur Regierung der Stadt Leipzig selbst nahmen die Eisenbahnbeh\u00f6rden die Ideen Lubowskis nicht so positiv auf wie erhofft. Unter anderem wollte er den Bayerischen Bahnhof komplett abrei\u00dfen und zwei Gleise im Vorgel\u00e4nde des Hauptbahnhofs absenken, um die Grundlage f\u00fcr eine unterirdische Streckenf\u00fchrung zu legen. Obwohl das Projekt in dieser Form noch nicht bef\u00fcrwortet worden war, begannen im Fr\u00fchjahr des Jahres 1914 die Arbeiten an einem etwa 700 Meter langen Tunnel, der sich im Bereich des \u00f6stlichen Vorgel\u00e4ndes des Hauptbahnhofs befand und der das erste St\u00fcck der in Planung befindlichen U-Bahn-Strecke bilden sollte. Noch heute sind Teile davon erhalten, ausgehend von der Osthalle und den Bahnsteigen 22 und 23. Da im Sommer 1914 der Erste Weltkrieg ausbrach wurden verst\u00e4ndlicherweise die weiteren Bauarbeiten gestoppt. Erst viele Jahre sp\u00e4ter wird mit dem modernen Projekt City-Tunnel die Ausstattung Leipzigs mit einer U-Bahn tats\u00e4chlich realisiert: Die Bauarbeiten starteten im Jahr 2003 und sollen Ende 2013 nun wirklich abgeschlossen werden. Hergestellt wird eine Verbindung zwischen Hauptbahnhof und Bayerischem Bahnhof. Unterquert wird eine Strecke von rund zwei Kilometern.<\/p>\n',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:43:51'
        },
        {
            id: '35',
            name: 'Brockhaus-Verlag',
            longitude: '12.385648',
            latitude: '51.340822',
            url: '',
            text: '<p>Leipzig w\u00fcrde wohl kaum den Namen \u00bbBuchstadt\u00ab tragen, g\u00e4be es nicht in der Stadtgeschichte allerlei namhafte Verlage zu erw\u00e4hnen. Neben Reclam und Kiepenheuer z\u00e4hlt der legend\u00e4re Brockhaus-Verlag zu den wichtigsten Verlagen des 19. und 20. Jahrhunderts. Friedrich Arnold Brockhaus (1772-1823) gr\u00fcndete die Sortiments- und Verlagsbuchhandlung "Rohloff & Co.", die er 1811 in Altenburg als \u00bbF. A. Brockhaus\u00ab neu gr\u00fcndete. 1817 zog er in die Buchstadt Leipzig, wo wenig sp\u00e4ter s\u00e4mtliche Werke gedruckt wurden. Nach dem Tod von Friedrich Arnold Brockhaus f\u00fchrten seine S\u00f6hne Heinrich und Friedrich Brockhaus die Gesch\u00e4fte fort. Die Firma begann zu expandieren. Im Mittelpunkt stand das \u00bbConversations-Lexicon\u00ab, eine riesige Enzyklop\u00e4die, die wir heute unter dem \u00bbGro\u00dfen Brockhaus\u00ab kennen. Au\u00dferdem wurden zahlreiche Biografien und Monografien gedruckt. Nach Ende des zweiten Weltkrieges wurde das Unternehmen zum \u00bbVEB Brockhaus Leipzig\u00ab verstaatlicht. Der derzeitige Inhaber Eberhard Brockhaus f\u00fchrte den privaten Verlag in Wiesbaden weiter. 1984 fusionierten beide Firmen in Mannheim zum \u00bbBibliographischen Institut & F. A. Brockhaus AG\u00ab. Nach der Wiedervereinigung nahm der Verlag in Leipzig seine T\u00e4tigkeit wieder auf, jedoch geschw\u00e4cht: einerseits durch die Verlegung des Hauptsitzes nach Mannheim und andererseits durch die immer gr\u00f6\u00dfer werdende \u00bbInternet-Welle\u00ab, unter der alle Printerzeugnisse leiden. 2008 wurde das traditionsreiche \u00bbBrockhaus-Zentrum\u00ab in Leipzig geschlossen.<\/p>\n',
            kategorie: '6',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:34:53'
        },
        {
            id: '36',
            name: 'Alte Handelsb\u00f6rse',
            longitude: '12.37585',
            latitude: '51.340771',
            url: '',
            text: '<p>Die Alte Handelsb\u00f6rse ist das \u00e4lteste Versammlungsgeb\u00e4ude der Stadt Leipzig. Dar\u00fcber hinaus eines der \u00e4ltesten Barockbauwerke der Stadt. Grund f\u00fcr den Bau des Geb\u00e4udes war ein Beschluss von Kaufleuten, welche den Abschluss gro\u00dfer Gesch\u00e4fte in einer neutralen Umgebung t\u00e4tigen wollten. Die Bauphase dauerte von 1678 bis 1687, wobei bereits 1679 eine erste Er\u00f6ffnung im noch unfertigen Zustand gefeiert wurde. H\u00f6chstwahrscheinlich entstand das Geb\u00e4ude nach den Pl\u00e4nen von Johann Georg Starke (Palais im gro\u00dfen Garten in Dresden) unter Leitung des Ratsmaurermeisters Christian Richter. Der Barockbau mit seinem unverwechselbaren Hang zur Renaissance ist Anlaufst\u00e4tte vieler Touristen und Kulturbegeisterter. Im Inneren ist die kunstvolle Stuckdecke von Giovanni Simonetti, einem der bedeutendsten Stuckateure seiner Zeit, zu bewundern. Zwischen 1699 und 1706 befand sich in den unterirdischen Gew\u00f6lben das erste Bankinstitut der Stadt - die Banco di Depositi. \u00dcber dem Haupteingang befindet sich ein Stadtwappen der Stadt Leipzig. Auf dem Dach, vier Sandsteinfiguren des Leipziger Bildhauers J. C. Sandtmann. [Merkur, Apollo,(Front) Minerva, Venus (Hinten) ] Zu dieser Zeit (Anfang des 18. Jahrhunderts) fanden drei Messen in Leipzig statt, wobei die Handelsb\u00f6rse von Kaufleuten aus Russland, Frankreich, Skandinavien, Griechenland und Italien genutzt wurde. Nach dem Messewachstum in der zweiten H\u00e4lfte des 19. Jahrhunderts war die Handelsb\u00f6rse den Anforderungen nicht mehr gewachsen - sie war schlichtweg zu klein. Daraufhin wurde der Bau einer neuen Handelsb\u00f6rse (damals Tr\u00f6ndlinring) beschlossen, welcher seinen Abschluss im Jahr 1886 fand. Durch zahlreiche Bombenangriffe im zweiten Weltkrieg auf Leipzig, wurde auch die Handelsb\u00f6rse in Mitleidenschaft gezogen. Die v\u00f6llig ausgebrannte B\u00f6rse wurde schlie\u00dflich ab 1955 wieder aufgebaut Und im Jahre 1962 fertiggestellt. Seitdem wird sie f\u00fcr zahlreiche Kongresse, Versammlungen und Konzerte genutzt.<\/p>\n',
            kategorie: '6',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:29:19'
        },
        {
            id: '37',
            name: 'Zentralmarkthalle',
            longitude: '12.377341',
            latitude: '51.334839',
            url: '',
            text: '<p>Die brache Fl\u00e4che am Wilhelm-Leuschner-Platz ist wohl jedem Leipziger bekannt. Sie ist die letzte, gro\u00dfe Fl\u00e4che in Leipzig, die seit der Zerst\u00f6rung im 2. Weltkrieg noch unbebaut ist. Dies soll sich 2013 \u00e4ndern: eine neue, moderne Markthalle soll stark angelehnt an die Architektur der alten Markthalle, auf diesem Platz gebaut werden.<\/p>\n',
            kategorie: '6',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 15:53:32'
        },
        {
            id: '38',
            name: 'Grossmarkthalle',
            longitude: '12.38937',
            latitude: '51.320669',
            url: '',
            text: '<p>Die Gro\u00dfmarkthalle in Leipzig, im Volksmund "Kohlrabizirkus" genannt f\u00e4llt bereits von weitem durch ihre gro\u00dfen Kuppeln auf. Bis 1995 war sie die wichtigste und gr\u00f6\u00dfte Markthalle in Leipzig. Heute wird dort jedoch nicht mehr mit Obst und Gem\u00fcse gehandelt. Bis 2010 konnte man dort im sogenannten "Eisdom" Schlittschuhlaufen. Heute finden hier gro\u00dfe Veranstaltungen wie Flohm\u00e4rkte, Messen und Konzerte statt. Durch seine enorme Gr\u00f6\u00dfe und die technische Ausstattung bietet er viele M\u00f6glichkeiten f\u00fcr die unterschiedlichsten Events.<\/p>\n',
            kategorie: '6',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:15:00'
        },
        {
            id: '40',
            name: 'Mercedes',
            longitude: '12.39754',
            latitude: '51.314484',
            url: '',
            text: '',
            kategorie: '6',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-29 00:10:28'
        },
        {
            id: '41',
            name: 'Pelzlstadt',
            longitude: '12.3',
            latitude: '51.3',
            url: '',
            text: '',
            kategorie: '6',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-28 17:01:30'
        },
        {
            id: '42',
            name: 'Kosumgenossenschaft',
            longitude: '12.329686',
            latitude: '51.328035',
            url: '',
            text: '',
            kategorie: '1',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2014-08-29 00:08:31'
        },
        {
            id: '43',
            name: 'Clara-Zetkin-Park',
            longitude: '12.359474',
            latitude: '51.331669',
            url: '',
            text: '<p>1955 wurden unter dem Namen "Zentraler Kulturpark Clara Zetkin" der Palmengarten, der Johannapark sowie der Scheibenholz-und Albertpark zusammengefasst. Trotz zahlreicher Proteste beschloss der Stadtrat im April 2011 die erneute Aufteilung der Gr\u00fcnanlage in Palmengarten, Johannapark sowie Klinger- und Richard-Wagner-Hain. Einen besonderen Anziehungspunkt stellt die Freilichtb\u00fchne mit diversen Konzertauff\u00fchrungen dar, das wieder er\u00f6ffnete Parkcaf\u00e9 l\u00e4dt zum gemeinsamen Plausch mit Freunden und Familie ein und an dem gro\u00dfen Spielplatz k\u00f6nnen sich vor allem die Kleinen erfreuen.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 19:52:16'
        },
        {
            id: '44',
            name: 'Friedenspark',
            longitude: '12.394689',
            latitude: '51.32737',
            url: '',
            text: '<p>Als zweiter st\u00e4dtischer Friedho,f auf einem 20 Hektar gro\u00dfen Gel\u00e4nde im Leipziger Zentrum-S\u00fcdost wurde der Friedenspark im Jahr 1846 er\u00f6ffnet. Der Architekt Hugo Licht steuerte die Pl\u00e4ne f\u00fcr den Bau der historischen Kapelle und Leichenhalle zwischen 1881 und 1884 bei, welche w\u00e4hrend des Zweiten Weltkrieges neben anderen Kunstwerken zerst\u00f6rt wurden. Am 20. Juli 1983 wurde die Parkanlage der \u00d6ffentlichkeit \u00fcbergeben, um dem Naherholungsbed\u00fcrfnis der B\u00fcrger in den angrenzenden Wohngebieten Ostvorstadt und Thonberg entgegenzukommen.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:12:38'
        },
        {
            id: '45',
            name: 'Johannapark',
            longitude: '12.363171',
            latitude: '51.334745',
            url: '',
            text: '<p>Zum Gedenken an seine verstorbene Tochter Johanna begr\u00fcndete der Leipziger Bankier Wilhelm Theodor Seyfferth 1858 eine Stiftung, welche er mit Hilfe des preu\u00dfischen Gartendirektors Peter Joseph Lenn\u00e9 in eine Parkanlage gestalten lie\u00df. Nach den Pl\u00e4nen des Architekten Julius Zeisig entstand von 1884 bis 1887 die nach dem Reformator benannte Lutherkirche, deren 58 Meter hoher Kirchturm das Leipziger Bachviertel ziert. 1955 wurde der Johannapark mit dem anschlie\u00dfenden Albertpark, dem Scheibenholzpark sowie dem Palmengarten unter dem Namen "Zentraler Kulturpark Clara Zetkin" zusammengefasst, erst im April 2011 erhielt die historische Anlage ihren urspr\u00fcnglichen Namen zur\u00fcck.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:30:38'
        },
        {
            id: '46',
            name: 'Lene-Voigt-Park',
            longitude: '12.395369',
            latitude: '51.335148',
            url: '',
            text: '<p>Auf dem Gel\u00e4nde des ehemaligen Eilenburger Bahnhofs entstand von 1999 bis 2004 ein ca. 11 Hektar gro\u00dfer Stadtteilpark, der f\u00fcr Jung und Alt vielf\u00e4ltige Freizeitm\u00f6glichkeiten zum Spielen und Erholen bietet. Im Jahr 2002 gewann der Lene-Voigt-Park den Europ\u00e4ischen Preis f\u00fcr Landschaftsarchitektur, um das b\u00fcrgerliche Engagement in den Foren und Workshops zu ehren, welche ma\u00dfgeblich zur Gestaltung der Parkanlage beigetragen haben.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:44:18'
        },
        {
            id: '47',
            name: 'Mariannenpark',
            longitude: '12.40441',
            latitude: '51.355145',
            url: '',
            text: '<p>Im Rahmen einer Ausschreibung zur Umgestaltung der Fl\u00e4che westlich der Lindenauer Allee konnte sich der Entwurf vom Landschaftsarchitekten Leberecht Migge im Jahr 1913 durchsetzen, welcher die Form eines modernen Volksgartens besa\u00df. Nicht zuletzt wegen seines reichhaltigen Spiel- und Sportangebotes wurde der Park 1991 in die Kulturdenkmalliste des Landes Sachsen aufgenommen. Als Ort zur Erholung f\u00fcr "alle Volksschichten" repr\u00e4sentiert der Mariannenpark eine neue soziale Politik sowie das Reformdenken dieser wichtigen Zeitepoche.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:49:07'
        },
        {
            id: '48',
            name: 'Palmengarten',
            longitude: '12.344821',
            latitude: '51.336008',
            url: '',
            text: '<p>Der Leipziger Landschaftsg\u00e4rtners Otto Mo\u00dfdorf wurde 1893 mit der Umsetzung des Entwurfs von Eduard May nach dem Vorbild von Frankfurt am Main beauftragt. Das Gesellschafts- und Konzerthaus diente lange Zeit als Kernpunkt und Besuchermagnet der Anlage, jedoch wurde es 1939 zerst\u00f6rt. Im Jahr 2008 er\u00f6ffnete das liebevoll gestaltete Revuetheater "Am Palmengarten" mit Restaurant und Kleinkunstb\u00fchne, welches Platz f\u00fcr bis zu 150 Personen bietet.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:41:51'
        },
        {
            id: '49',
            name: 'Rosental',
            longitude: '12.364736',
            latitude: '51.348593',
            url: '',
            text: '<p>Ab 1837 begann die Gestaltung der ehemaligen barocken Anlage zum Landschaftspark nach einem Entwurf des Ratsg\u00e4rtners Rudolph Siebeck, wobei ein Teil des Auenwaldes und zahlreiche Tier- und Pflanzenarten dem Park erhalten blieben. Die Gr\u00fcndung des Zoologischen Gartens erfolgte 1878 durch den Privatmann Ernst Pinkert. Der Einblick in den Tierbestand des Zoos und Ausblick f\u00fcr Zoobesucher in die Rosentallandschaft wurde mit der Fertigstellung des Zoo-Schaufensters 1976 erm\u00f6glicht. In den Jahren 1887 bis 1896 entstand durch die Aufsch\u00fcttung von Hausm\u00fcll der 20 Meter hohe Rosentalh\u00fcgel ("Scherbelberg"), dessen Aussichtsturm mit einer malerischen Aussicht in das Rosental zum Verweilen einl\u00e4dt.<\/p>\n',
            kategorie: '5',
            public: '0',
            last_edit_user_id: '2',
            last_edit_time: '2015-09-01 20:26:55'
        }
    ];

    ionViewDidEnter() {
        this.loadmap();
    }

    loadmap() {
        this.map = leaflet.map('map').fitWorld();
        for (let i = 0; i < this.markers.length; ++i) {
            leaflet.marker([this.markers[i].latitude, this.markers[i].longitude])
                .bindPopup('<a href="' + this.markers[i].url + '" target="_blank">' + this.markers[i].name + '</a>' + this.markers[i].text + '</a>')
                .addTo(this.map);
        }
        leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            // tslint:disable-next-line:max-line-length
            attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18
        }).addTo(this.map);
        this.map.locate({
            setView: true,
            maxZoom: 10
        }).on('locationfound', (e) => {
            const markerGroup = leaflet.featureGroup();
            const marker: any = leaflet.marker([e.latitude, e.longitude]).on('click', () => {
                alert('Marker clicked');
            });
            markerGroup.addLayer(marker);
            this.map.addLayer(markerGroup);
        }).on('locationerror', (err) => {
            alert(err.message);
        });

    }

    ngOnInit(): void {

    }
}


// import {Component, ViewChild, ElementRef, OnInit} from '@angular/core';
// import {NavController} from '@ionic/angular';
// import leaflet from 'leaflet';
// import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
// import {Geolocation} from '@ionic-native/geolocation/ngx';
// import {GeolocationOptions, Geoposition} from '@ionic-native/geolocation';
// import {Map, latLng, tileLayer, Layer, marker} from 'leaflet';
// import {load} from '@angular/core/src/render3';
//
// @Component({
//     selector: 'app-mobile-guide',
//     templateUrl: './mobile-guide.page.html',
//     styleUrls: ['./mobile-guide.page.scss'],
// })
// export class MobileGuidePage implements OnInit {
//     @ViewChild('map') mapContainer: ElementRef;
//     map: any;
//     options: GeolocationOptions;
//     currentPos: Geoposition;
//
//     // constructor(private androidPermissions: AndroidPermissions, public navCtrl: NavController, private geolocation: Geolocation) {
//     // }
//     constructor(public navCtrl: NavController, private geolocation: Geolocation) {
//     }
//
//     ionViewDidEnter() {
//         this.loadmap();
//     }
//
//     getUserPosition() {
//         this.options = {
//             enableHighAccuracy: true
//         };
//
//         this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
//
//             this.currentPos = pos;
//             console.log(pos);
//
//         }, (err: PositionError) => {
//             console.log('error : ' + err.message);
//         });
//     }
//
//     // loadmap() {
//     //
//     //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
//     //         result => console.log('Has permission?', result.hasPermission),
//     //         err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
//     //     );
//     //
//     //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
//     //
//     //     this.options = {
//     //         enableHighAccuracy: true
//     //     };
//     //
//     //     this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
//     //
//     //         this.currentPos = pos;
//     //         console.log(pos);
//     //         this.map = new Map('map').setView([this.currentPos.coords.latitude, this.currentPos.coords.longitude], 8);
//     //
//     //         tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     //             // tslint:disable-next-line
//     //             attribution: 'Map data &copy; <ahref="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <ahref="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     //             maxZoom: 18
//     //         }).addTo(this.map);
//     //     }, (err: PositionError) => {
//     //         console.log('error : ' + err.message);
//     //     });
//     // }
//
//     loadmap() {
//         this.map = leaflet.map('map').fitWorld();
//         leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//             attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
//             maxZoom: 18
//         }).addTo(this.map);
//         this.map.locate({
//             setView: true,
//             maxZoom: 10
//         }).on('locationfound', (e) => {
//             // const markerGroup = leaflet.featureGroup();
//             // const marker: any = leaflet.marker([e.latitude, e.longitude]).on('click', () => {
//             //     alert('Marker clicked');
//             // });
//             // markerGroup.addLayer(marker);
//             // this.map.addLayer(markerGroup);
//         }).on('locationerror', (err) => {
//             alert(err.message);
//         });
//
//     }
//
//     ngOnInit(): void {
//
//     }
// }
