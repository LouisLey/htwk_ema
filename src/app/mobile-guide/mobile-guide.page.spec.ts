import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MobileGuidePage} from './mobile-guide.page';

describe('MobileGuidePage', () => {
  let component: MobileGuidePage;
  let fixture: ComponentFixture<MobileGuidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileGuidePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileGuidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
