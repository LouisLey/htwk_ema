import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouteReuseStrategy, RouterModule, Routes} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {MobileGuidePage} from './mobile-guide.page';
import {Geolocation} from '@ionic-native/geolocation/ngx';

const routes: Routes = [
    {
        path: '',
        component: MobileGuidePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [MobileGuidePage],
    providers: [
        Geolocation,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ]
})
export class MobileGuidePageModule {
}
