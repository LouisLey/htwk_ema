import {Component, OnInit} from '@angular/core';
import {RaumplanungService} from '../../service/raumplanung.service';

@Component({
    selector: 'app-event',
    templateUrl: './event.page.html',
    styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {
    von: any;
    bis: any;
    bem: any;
    raum: any;


    constructor(private raumplanungService: RaumplanungService,
    ) {
    }

    ngOnInit() {

    }

    newReservationForm() {
        console.log(this.von);
        console.log(this.bis);
        console.log(this.bem);
        console.log(this.raum);
        this.raumplanungService.postReservation('ce683ac30c6c8.63476780', this.von, this.bis, this.raum);
    }

    private goBack() {
    }
}
