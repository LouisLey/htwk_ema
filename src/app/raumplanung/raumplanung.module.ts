import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {RaumplanungService} from '../service/raumplanung.service';
import {IonicCalendarPage, MaterialformPage} from './ionicCalendar/ionicCalendar.page';
import {LoginPage} from './login/login.page';
import {FullCalendarModule} from '@fullcalendar/angular';
import {IonicModule} from '@ionic/angular';
import {NgCalendarModule} from 'ionic2-calendar';
import {CalendarPage} from './calendar/calendar.page';
import {MyRoomsPage} from './my-rooms/my-rooms.page';
import {TransfereService} from '../service/transfer.service';
import {MatDialogModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {EventPage} from './event/event.page';

const routes: Routes = [
    {
        path: 'raumplanung',
        component: LoginPage,
    },
    {
        path: 'calendar',
        component: CalendarPage
    },
    {
        path: 'ionic-calendar',
        component: IonicCalendarPage
    },
    {
        path: 'myrooms',
        component: MyRoomsPage
    },
    {
        path: 'event',
        component: EventPage
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        FullCalendarModule,
        IonicModule,
        NgCalendarModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        MatInputModule
    ],
    exports: [
        RouterModule
    ],
    providers: [
        RaumplanungService, TransfereService
    ],
    entryComponents: [MaterialformPage], bootstrap: [MaterialformPage],

    declarations: [ CalendarPage, IonicCalendarPage, LoginPage, MyRoomsPage, MaterialformPage, EventPage]
})
export class RaumplanungPageModule {
}
