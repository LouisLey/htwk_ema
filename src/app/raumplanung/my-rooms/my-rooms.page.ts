import {Component, OnInit} from '@angular/core';
import {RaumplanungService} from '../../service/raumplanung.service';
import {MyroomsEntity} from '../../models/MyRoom';

@Component({
    selector: 'app-my-rooms',
    templateUrl: './my-rooms.page.html',
    styleUrls: ['./my-rooms.page.scss'],
})
export class MyRoomsPage implements OnInit {

    public DateList: Array<MyroomsEntity> = [];

    ngOnInit() {
        this.raumplanungService.getMyRooms('5ce683ac30c6c8.63476780').subscribe(res => {
            console.log(res.myrooms);
            this.DateList = res.myrooms;
        }, err => {
            console.log(err);
        });
    }

    constructor(
        private raumplanungService: RaumplanungService,
    ) {
    }
}
