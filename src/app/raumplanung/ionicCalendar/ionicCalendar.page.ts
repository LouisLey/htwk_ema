import {Component, Inject, OnInit} from '@angular/core';
import {RaumplanungService} from '../../service/raumplanung.service';
import {ReservationsEntity} from '../../models/reservierung';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {TransfereService} from '../../service/transfer.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

// import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-calendar',
    templateUrl: './ionicCalendar.page.html',
    styleUrls: ['./ionicCalendar.page.scss'],
})
export class IonicCalendarPage implements OnInit {
    name: string;
    model = {vorname: '', name: '', deviceid: '', email: ''};
    reservierungsEntity: ReservationsEntity[];
    eventSource;
    viewTitle;
    isToday = true;
    user = this.transfereService.getData();
    calendar = {
        mode: 'month',
        currentDate: new Date()
    }; // these are the variable used by the calendar.
    ngOnInit(): void {
    }

    constructor(
        private storage: Storage,
        private transfereService: TransfereService,
        private raumplanungService: RaumplanungService,
        private dialog: MatDialog,
        private router: Router,
        // public alertController: AlertController
    ) {
        this.eventSource = this.loadReservation();
    }

    loadEvents() {
        this.eventSource = this.loadReservation();
    }

    loadMyRooms() {
        console.log('ich bin hier');
        this.router.navigate(['/myrooms']);
    }

    newReservation() {
        this.router.navigate(['/event']);
    }

    onViewTitleChanged(title) {
        this.viewTitle = title;
    }

    onEventSelected(event) {
        console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    }

    changeMode(mode) {
        this.calendar.mode = mode;
    }

    today() {
        this.calendar.currentDate = new Date();
    }

    // close() {
    //     this.dialogRef.close();
    // }
    //
    // save() {
    //     this.dialogRef.close(this.form.value);
    // }

    onTimeSelected(ev) {
        // this.router.navigate(['/event']);
    }

    onCurrentDateChanged(event: Date) {
        const today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }

    private loadReservation() {
        this.raumplanungService.getReservationsWithRoomID('1', this.user.deviceid).subscribe(res => {
            console.log(res);
            this.reservierungsEntity = res.reservations;
            this.eventSource = this.reservierungsEntity.map((e) => {
                const cEvent: any = Object.assign({}, e);
                cEvent.startTime = new Date(e.Von);
                cEvent.endTime = new Date(e.Bis);
                cEvent.title = e.Name;
                return cEvent;
            });
        }, err => {
            console.log(err);
            this.reservierungsEntity = [];
        });
    }

    onRangeChanged(ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    }

    markDisabled = (date: Date) => {
        const current = new Date();
        current.setHours(0, 0, 0);
        return date < current;
    };
}


// todo: funktioniert nicht. Dialog poppt immer auf und Datepicker nicht möglich?
// async onTimeSelected(ev) {
// const alert = await this.alertController.create({
//     header: 'Eigenen Raum reservieren?',
//     inputs: [
//         {
//             name: 'raum',
//             type: 'text',
//             id: 'name2-id',
//             value: 'Raum?',
//             placeholder: 'Placeholder 2'
//         },
//         {
//             name: 'datum',
//             type: 'date',
//         },
//         {
//             name: 'begin',
//             type: 'number',
//             placeholder: '0900',
//             max: 4
//         },
//         {
//             name: 'ende',
//             type: 'number',
//             placeholder: '0940',
//             max: 4
//         }
//     ],
//     buttons: [
//         {
//             text: 'Cancel',
//             role: 'cancel',
//             cssClass: 'secondary',
//             handler: () => {
//                 console.log('Confirm Cancel');
//             }
//         }, {
//             text: 'Ok',
//             handler: data => {
//                 console.log('Confirm Ok' + JSON.stringify(data));
//             }
//         }
//     ]
// });
//
// await alert.present();
// }

// todo: funktioniert nicht. Dialog poppt nur kurz auf
// onTimeSelected(ev) {
//     const dialogRef = this.dialog.open(MaterialformPage, {
//         width: '250px',
//         data: {name: this.name, animal: this.animal}
//     });
//
//     dialogRef.afterClosed().subscribe(result => {
//         console.log('The dialog was closed');
//         this.animal = result;
//     });
//
//     // this.raumplanungService.postReservation(this.user.deviceid, ev.selectedTime, ev.selectedTime, '0');
//     console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
//         (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
// }


// todo: funktioniert nicht. Dialog poppt nur kurz auf
export interface DialogData {
    animal: string;
    name: string;
}

// todo: funktioniert nicht. Dialog poppt nur kurz auf
@Component({
    selector: 'app-materialform',
    templateUrl: './materialform.page.html',
})
export class MaterialformPage implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<MaterialformPage>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit(): void {
    }
}
