import {Component, OnInit} from '@angular/core';
import {RaumplanungService} from '../../service/raumplanung.service';
import {HttpClient} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';
import {TransfereService} from '../../service/transfer.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    model = {vorname: '', name: '', deviceid: '', email: ''};
    submitted = false;

    constructor(
        private transfereService: TransfereService,
        private raumplanungService: RaumplanungService,
        private http: HttpClient,
        private storage: Storage,
        private router: Router,
    ) {
        // Or to get a key/value pair
        this.storage.get('deviceid').then((val) => {
            this.model.deviceid = val;
            console.log(val);
        });
        this.storage.get('email').then((val) => {
            this.model.email = val;
            console.log(this.model);
        });
        this.storage.get('name').then((val) => {
            this.model.name = val;
            console.log(val);
        });
        this.storage.get('vorname').then((val) => {
            this.model.vorname = val;
            console.log(this.model);
        });
    }

    ngOnInit() {

    }

    onSubmit() {
        this.submitted = true;
        this.raumplanungService.register(this.model);
    }

    async newUser() {
        // console.log(this.model);
        const data = await this.raumplanungService.register(this.model).toPromise();
        this.model.deviceid = data.deviceid;
        console.log(this.model);
        this.storage.set('deviceid', this.model.deviceid);
        this.storage.set('email', this.model.email);
        this.storage.set('name', this.model.name);
        this.storage.set('vorname', this.model.vorname);


        this.transfereService.setData(this.model);
        this.router.navigate(['/ionic-calendar']);

    }
}
