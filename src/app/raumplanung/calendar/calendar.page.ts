import {Component, OnInit, ViewChild} from '@angular/core';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {EventInput} from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction'; // for dateClick
import timeGridPlugin from '@fullcalendar/timegrid';
import {RaumplanungService} from '../../service/raumplanung.service';
import {Storage} from '@ionic/storage';
import {HttpClient} from '@angular/common/http';
import {ReservationsEntity, Reservierung} from '../../models/reservierung';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.page.html',
    styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage implements OnInit {
    @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template
    calendarVisible = true;
    calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];
    calendarWeekends = true;
    calendarEvents: EventInput[] = [];

    model = {vorname: '', name: '', deviceid: '', email: ''};
    reservierung: Reservierung;
    reservierungsEntity: ReservationsEntity[];

    constructor(
        private raumplanungService: RaumplanungService,
        private http: HttpClient,
        private storage: Storage,
    ) {
    }

    ngOnInit() {
        // Or to get a key/value pair
        this.storage.get('deviceid').then((val) => {
            this.model.deviceid = val;
        });
        this.storage.get('email').then((val) => {
            this.model.email = val;
        });
        this.storage.get('name').then((val) => {
            this.model.name = val;
        });
        this.storage.get('vorname').then((val) => {
            this.model.vorname = val;
        });

        this.loadReservation();
    }

    private async loadReservation() {
        this.raumplanungService.getReservationsWithRoomID('1', '5ce683ac30c6c8.63476780').subscribe(res => {
            this.reservierung = res;
            this.reservierungsEntity = this.reservierung.reservations;

            const eventsToAdd = this.reservierungsEntity.map((e) => {
                const cEvent: any = Object.assign({}, e);
                cEvent.start = e.Von.split(' ')[0];
                cEvent.title = e.Name;
                return cEvent;
            });
            console.log('events to add', eventsToAdd);
            this.calendarEvents = this.calendarEvents.concat(eventsToAdd);
        }, err => {
            console.log(err);
            this.reservierung = null;
            this.reservierungsEntity = [];
        });
    }

    toggleVisible() {
        this.calendarVisible = !this.calendarVisible;
    }

    toggleWeekends() {
        this.calendarWeekends = !this.calendarWeekends;
    }

    gotoPast() {
        const calendarApi = this.calendarComponent.getApi();
        calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
    }

    handleDateClick(arg) {
        if (confirm('Would you like to add an event to ' + arg.dateStr + ' ?')) {
            this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
                title: 'New Event',
                start: arg.date,
                allDay: arg.allDay
            });
        }
    }
}
