// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,

    firebase: {
        apiKey: 'AIzaSyDJZHb726TFa0y10q_XdTJHEbidXqWa_2I',
        authDomain: 'pushtests-9dff0.firebaseapp.com',
        databaseURL: 'https://pushtests-9dff0.firebaseio.com',
        projectId: 'pushtests-9dff0',
        storageBucket: 'pushtests-9dff0.appspot.com',
        messagingSenderId: '896353835710',
        appId: '1:896353835710:web:997277186cb2fff1',
        vapidKey: 'BOSTBRfhMiP7BM2d8ZV85_RnAoPuALJnmeKtKxaPbMg5MxQIYjMILBRrQdpRSwqzmBCIKXW1ozOX0c-RfxLYXyo'
    }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
